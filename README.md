arch-installer is an Arch Linux installer written in python.



How to use:
1. Download arch linux iso from [https://www.archlinux.org/download](https://www.archlinux.org/download)
2. Make a bootable usb or cd. If installing in a virtual machine, you can skip this step.
3. Boot arch iso(check step 4 before doing this).
4. Get arch-installer. There are a couple ways for doing this.  
    a)  when in iso boot menu, press tab and type ```cow_spacesize=1G``` and then enter.
        You will first need internet connection. If you are using a wired connection, it should work out of the box. If using WiFi, use the ```wifi-menu``` command to connect to the desired network.
        After you are connected, install git by typing ```pacman -Sy git```
        Then clone this repository by typing ```git clone https://gitlab.com/Bettehem/arch-installer.git``` and then enter the directory with ```cd arch-installer```
    
    b)  the other option is to use the wget tool to download the installer.
        You will first need internet connection. If you are using a wired connection, it should work out of the box. If using WiFi, use the ```wifi-menu``` command to connect to the desired network.
        After you are connected, download the installer using ```wget https://gitlab.com/Bettehem/arch-installer/raw/master/arch-install.py```
5. Run the installer. You can do this in two ways  
    a)  first make the file executable with ```chmod +x arch-install.py``` and then run it with ```./arch-install.py```
    
    b)  or just run it with ```python arch-install.py```