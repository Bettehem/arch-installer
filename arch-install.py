#!/bin/python
import subprocess
from pathlib import Path




print("Arch Linux installer\n\n")

#main function
def main():
    print("Select mode:")
    while True:
        print("\n1. Easy mode\n2. Advanced mode\nEnter number: ", end="")
        mode = subprocess.getoutput('echo -e "#!/bin/bash\nread -n 1 -s input\necho \$input" > .mode; bash .mode; rm .mode')
        if mode in ['1', '2']:
            installer(mode)
            break
        else:
            print("Enter a valid mode\n")

#selects installer based on inputted mode
def installer(mode):
    #1 = easymode, 2 = advanced mode
    if mode == '1':
        easyMode()
    else:
        advancedMode()


def easyMode():
    print("Arch Linux Installer: EASY MODE\n\n")
    #check if first time
    if not Path(".progress").exists():
        print("""Welcome to Arch Linux installer.\n
            This installer will guide you through installing Arch Linux on to your system.\n
            You can exit out of the installer with CTRL+C and resume the installation when you want.\n""")
    else:
        resumeInstall()


def advancedMode():
    print("nothing here yet!\nredirecting to easy mode...")
    easyMode()



def resumeInstall():
    #get progress
    file = open(".progress", "r")
    progress = file.read()
    

'''
print("Select device(s) for installation:")
subprocess.run("lsblk")
subprocess.run(["lsblk", "-f"])
print("\nwhich device(s) are you going to use?:")
print("example:  /dev/sda, /dev/sdc")
print("example:  sda, sdc")

#get devices
devices = input("enter device(s): ").split(",")

if len(devices) < 1:
    for i, dev in enumerate(devices):
        if "/dev" not in dev:
            devices[i] = "/dev/" + dev
        
        continue

    print("selected " + ", ".join(devices))
else:
    if len(devices) == 1:
        if " " in devices:
            devices = devices.replace(" ", ",").split(",")
            
            for i, dev in enumerate(devices):
                if "/dev" not in dev:
                    devices[i] = "/dev/" + dev
                continue
    print("selected " + ", ".join(devices))


'''
main()

